var yearLabels = ["Year", "Ann\u00e9e","Année"];
var periodLabel = ["Period"];
var weekLabels = ["Week", "Semaine","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
var monthLabels = ["January", "February","March","April","May","June","July","August","September","October","November","December"];
var dataElementBase = "";
var orgUnitBase = "";
var periodBase = "";


function searchYearPeriod(worksheet){
    var periodList = new Array();
    console.log(" searchYearPeriod() method ");
    periodList = getPeriodList(worksheet, yearLabels, "Y");
    if(periodList.length == 0)
    {
        showWarning( "Please ener valid year, in YYYY pattern before clicking 'Import DataValues'.");
    }
}
/**
 * Searches period position in worksheet depends on type of period.
 * @param {Object} worksheet
 * @returns {List}periodList
 */
function searchPeriodPosition(worksheet) {

    var periodList = new Array();
    
    //1. lets check if excel have period text
    periodList = getPeriodList(worksheet, periodLabel);

    if (periodList.length == 0)
    {
        //2. lets check if excel have weeks
        periodList = getPeriodList(worksheet, weekLabels, "W");
        if (periodList.length == 0)
        {
            //3. lets check if excel have months
            periodList = getPeriodList(worksheet, monthLabels, "M");
        }
    }
    return periodList;
}

/**
 * Searches periods in one worksheet by matching values in the list(e.g. Months name list, Weeks name list).
 * @param {Object} worksheet
 * @param {List} list
 * @returns {List}periodList
 */
function getPeriodList(worksheet, list, type) {
    var periodList = [];
    var dataValues = [];
    var dataRows = [];
    var dataCols = [];

    var wSDataStr = JSON.stringify(worksheet.data);
    $.each(list, function(wkIndex, listValue) {
        
        if (wSDataStr.indexOf(listValue) !== -1) {
            console.log(listValue);
            var tempDataValues = getMatchingPeriods(wSDataStr, listValue,false,type);
            for (row = 0; row < tempDataValues.length; row++) {
                if (tempDataValues[row] != undefined && tempDataValues[row] != "") {
                    if (dataRows.indexOf(row) == -1)
                        dataRows[dataRows.length] = row;
                    //alert("row is array = "+(tempDataValues[row] instanceof Array) +" row = "+ row + " value "+tempDataValues[row]);

                    if (dataValues[row] == undefined)
                        dataValues[row] = [];

                    for (col = 0; col < tempDataValues[row].length; col++) {
                        if (tempDataValues[row][col] != undefined && tempDataValues[row][col] != "") {
                            if (dataCols.indexOf(col) == -1)
                                dataCols[dataCols.length] = col;

                            dataValues[row][col] = tempDataValues[row][col];
                            //alert(" row = "+(row+1) + " col = "+ (col+1) + " value  = "+tempDataValues[row][col]);
                        }
                    }
                }
            }
        }
    });

    //alert("datarows = "+dataRows.length + " dataCols "+dataCols.length);
    var nextIndex = 0;
    if (dataRows.length == 1 && dataCols.length != 1)
    {
        periodBase = "colBased";
        var maxColDiff = 0;
        for (i = 0; i < dataCols.length; i++) {
            if (dataCols[i] != undefined && dataCols[i] != "") {
                var colNo = dataCols[i];
                periodList[colNo] = dataValues[dataRows[0]][colNo];
                /*if more than one col have same period and cells are merged
                *then only first col shows correct period in json and other merged cells give null values
                *So following code adds same period in remaining merged columns.
                */
                nextIndex = colNo + 1;
                if(i==0)
                    maxColDiff = dataCols[i+1] - colNo;
                var index = 0 + maxColDiff;
                while(index > 1 && nextIndex<worksheet.maxCol && dataValues[dataRows[0]][nextIndex]==undefined)
                {
                    periodList[nextIndex] = dataValues[dataRows[0]][colNo];
                    index--;
                    nextIndex++;
                }
                //console.log("colNo "+colNo +" periodList = "+periodList);
            }
        }
    }
    else if (dataRows.length != 1 && dataCols.length == 1) {
        var maxRowDiff = 0;
        periodBase = "rowBased";
        for (i = 0; i < dataRows.length; i++) {
            if (dataRows[i] != undefined && dataRows[i] != "") {
                var rowNo = dataRows[i];
                periodList[rowNo] = dataValues[rowNo];
                /*if more than one row have same period and cells are merged
                *then only first row shows correct period in json and other merged cells give null values
                *So following code adds same period in remaining merged rows.
                */
                nextIndex = rowNo + 1;
                if(i==0)
                    maxRowDiff = dataRows[i+1] - rowNo;
                
                var index = 0 + maxColDiff;
                while(index > 1 && nextIndex<worksheet.maxRow && dataValues[nextIndex]==undefined)
                {
                    periodList[nextIndex] = dataValues[rowNo];
                    nextIndex++;
                    index--;
                }
                //console.log("row "+rowNo+ " periodList = "+periodList);
            }
        }
    }
    else if (dataRows.length == 1 && dataCols.length == 1) {
        //check next two cols for period
        var pattern = /[0-9]+/;
        var nextCol = getValue(wSDataStr, dataRows[0], (dataCols[0]+1));
        var nextRow = getValue(wSDataStr, (dataRows[0]+1), dataCols[0]);
        if(nextCol.match(pattern) != undefined && nextCol.match(pattern) > 1990)
            periodList[0] = nextCol.match(pattern);
        else if(nextRow.match(pattern) != undefined && nextRow.match(pattern) > 1990)
            periodList[0] = nextRow.match(pattern);
    }
    
    return periodList;
}

/**
 * Return matching periods inside data
 * @param {String} data (String object of worksheet)
 * @param {String} value (Value which should be mathced)
 * @returns {boolean} same (matches exact same value in data if its true)
 */
function getMatchingPeriods(data, value, same,type) {
    var valueList = [];
    var splitedRows = data.split("]");
    for (r = 0; r < splitedRows.length; r++)
    {
        var row = splitedRows[r];
        if (row.indexOf(value) !== -1)
        {

            valueList[r] = [];
            if(row.indexOf("[null,")!= -1)
            {
               row = row.replace("[null,",'[{"value":null,"formatCode":"General"},');
            }
            if(row.indexOf(",null,")!= -1)
            {
               row = row.replace(",null,",'[{"value":null,"formatCode":"General"},');
            }
            var splitedCols = row.split("{");
            for (c = 0; c < splitedCols.length; c++)
            {

                var index = splitedCols[c].indexOf(value);
                if (index !== -1)
                {
                    var str = splitedCols[c].substring(index);
                    var tempVal = str.substring(0, str.indexOf(",") - 1);
                    if(type=="W")
                    {
                        var pattern = /[0-9]+/;
                        //console.log(tempVal + " weekno "+tempVal.match(pattern));
                        tempVal = "W"+tempVal.match(pattern);
                    }
                    if(same)
                    {
                        if(value==tempVal)
                            valueList[r][(c - 1)] = tempVal;
                    }
                    else
                        valueList[r][(c - 1)] = tempVal;
                    //console.log("matchingvalue = "+value + " value "+tempVal);
                    //console.log("row = "+(r+1)+ " col = "+(c) + " value = "+str.substring(0,str.indexOf(",")-1));
                }
            }
        }
    }
    //alert("valueList "+valueList);
    return valueList;
}

/**
 * Searches organisation unit in one worksheet by matching values in the orgUnits List(list of all the organisation units present in DB).
 * @param {Object} worksheet
 * @param {List} orgUnitsList
 * @returns {List}orgUnitInWorkbookList
 */
function searchOrgUnitPosition(worksheet, orgUnitsList) {
    var orgUnitInWorkbookList = new Array();
    var dataValues = [];
    var dataRows = [];
    var dataCols = [];

    var wSDataStr = JSON.stringify(worksheet.data);

    $.each(orgUnitsList, function(ouIndex, ouLabel) {
        var matchOuType = $("input[name='ouType']:checked").val() == "code" ? ouLabel.code : ouLabel.name;
        if (wSDataStr.indexOf(matchOuType) !== -1) {
            //console.log("Matached Ou = "+ouLabel.name);

            var tempDataValues = getMatchingValues(wSDataStr, matchOuType,true);

            for (row = 0; row < tempDataValues.length; row++) {
                if (tempDataValues[row] != undefined && tempDataValues[row] != "") {
                    if (dataRows.indexOf(row) == -1)
                        dataRows[dataRows.length] = row;
                    //alert("row is array = "+(tempDataValues[row] instanceof Array) +" row = "+ row + " value "+tempDataValues[row]);

                    if (dataValues[row] == undefined)
                        dataValues[row] = [];

                    for (col = 0; col < tempDataValues[row].length; col++) {
                        if (tempDataValues[row][col] != undefined && tempDataValues[row][col] != "") {
                            if (dataCols.indexOf(col) == -1)
                                dataCols[dataCols.length] = col;

                            //dataValues[row][col] = tempDataValues[row][col];
                            dataValues[row][col] = ouLabel.name;
                            //console.log("Matached Ou = "+ouLabel.name +" row = "+(row+1) + " col = "+ col + " value  = "+dataValues[row][col]);
                        }
                    }
                }
            }
        }
    });
    var nextIndex = 0;
    //console.log(dataRows.length + " "+dataCols.length);
    if (dataRows.length == 1 && dataCols.length != 1)
    {
        orgUnitBase = "colBased";
        for (i = 0; i < dataCols.length; i++) {
            orgUnitInWorkbookList[dataCols[i]] = dataValues[dataRows[0]][dataCols[i]];
        }
    }
    else if (dataRows.length != 1 && dataCols.length == 1) {
        orgUnitBase = "rowBased";
        dataRows.sort();
        for (i = 0; i < dataRows.length; i++) {
            orgUnitInWorkbookList[dataRows[i]] = dataValues[dataRows[i]][dataCols[0]];
        }
    }
    else if (dataRows.length == 1 && dataCols.length == 1) {
        orgUnitInWorkbookList[0] = dataValues[dataRows[0]][dataCols[0]];
        alert("Having single orgunit");
    }
    else if (dataRows.length == 0 && dataCols.length == 0) {
        alert("Orgunit is missing in the sheet.");
    }
    else if(dataRows.length > 1 && dataCols.length > 1)
    {
        var orgUnitNames = "";
    
        for (i = 0; i < dataRows.length; i++) {
            for(j = 0; j < dataCols.length; j++) 
            {
                if(dataValues[dataRows[i]][dataCols[j]]!=undefined)
                {
                    if(orgUnitNames=="")
                        orgUnitNames = dataValues[dataRows[i]][dataCols[j]];
                    else
                        orgUnitNames = orgUnitNames + ","+dataValues[dataRows[i]][dataCols[j]];
                }
            }
        }
        alert("Datasheet have '"+orgUnitNames + "' orgunits. These Organisation units are not in a single row or column.");
    }

    //console.log(" returning  orgUnitInWorkbookList = "+orgUnitInWorkbookList);
    return orgUnitInWorkbookList;
}

/**
 * Searches dataelements in one worksheet by matching values in the data element List(list of all the data elements present in DB).
 * @param {Object} worksheet
 * @param {List} dEList
 * @returns {List} dataElementInWorkbookList
 */
function searchDataElementPosition(worksheet, dEList) {
    //array of all the data elements present in sheet.
    var dataElementInWorkbookList = new Array();
    var dataValues = [];
    var dataRows = [];
    var dataCols = [];

    var wSDataStr = JSON.stringify(worksheet.data);
    //console.log("wSDataStr = "+wSDataStr);
    //right now by default I have kept dataelement name as sheetname + dename mentioned in sheet.
    // @TODO: Option should be provided to user.
    $.each(dEList, function(deIndex, dELabel) {
        

        if(dELabel.name != undefined)
        {
            //Data element name is worksheet name + name present in cells of sheet.
            var dataElementName = dELabel.name.replace((worksheet.name) + " ", "");
            //var dataElementName = dELabel.code;
            //console.log("dataElementName = "+dataElementName + " name = "+dELabel.name +" sheet name "+worksheet.name);

            if (dELabel.name.indexOf(worksheet.name) !== -1 && wSDataStr.indexOf(dataElementName) !== -1)
            {
                //console.log("worksheet name "+worksheet.name + " dELabel "+dELabel.name);
                console.log("Matached de = "+dataElementName);

                //Search for matching values array with dataelement name in worksheet 'wSDataStr' and 'true' means value should be exact same.
                var tempDataValues = getMatchingValues(wSDataStr, dataElementName,true);

                for (row = 0; row < tempDataValues.length; row++) {
                    if (tempDataValues[row] != undefined && tempDataValues[row] != "") {
                        if (dataRows.indexOf(row) == -1)
                            dataRows[dataRows.length] = row;
                        //alert("row is array = "+(tempDataValues[row] instanceof Array) +" row = "+ row + " value "+tempDataValues[row]);

                        if (dataValues[row] == undefined)
                            dataValues[row] = [];
                        if (dataElementInWorkbookList[row] == undefined)
                            dataElementInWorkbookList[row] = [];

                        for (col = 0; col < tempDataValues[row].length; col++) {
                            if (tempDataValues[row][col] != undefined && tempDataValues[row][col] != "") {
                                if (dataCols.indexOf(col) == -1)
                                    dataCols[dataCols.length] = col;

                                //dataValues[row][col] = tempDataValues[row][col];
                                dataValues[row][col] = dELabel.name+"|"+dELabel.code;
                                dataElementInWorkbookList[row][col] = dataValues[row][col];
                                //console.log(" row = "+(row) + " col = "+ col + " value  = "+dataElementInWorkbookList[row][col]);
                            }
                        }
                    }
                }
            }
        }
    });
    //alert("datarows = "+dataRows.length + " dataCols "+dataCols.length);
    if (dataRows.length == 1 && dataCols.length != 1)
    {
        dataElementBase = "colBased";
    }
    else if (dataRows.length != 1 && dataCols.length == 1) {
        dataElementBase = "rowBased";
    }
    else if (dataRows.length == 1 && dataCols.length == 1) {
        alert("Having single Data Element.");
    }
    else if (dataRows.length == 0 && dataCols.length == 0) {
        alert("Data Element is missing in the sheet.");
    }

    dataElementInWorkbookList = dataValues;

    //alert(" returning  dataElementInWorkbookList = "+dataElementInWorkbookList);
    return dataElementInWorkbookList;
}

/**
 * Return matching value inside data
 * @param {String} data (String object of worksheet)
 * @param {String} value (Value which should be mathced)
 * @returns {boolean} same (matches exact same value in data if its true)
 */
function getMatchingValues(data, value, same) {
    var valueList = [];
    var splitedRows = data.split("]");
    for (r = 0; r < splitedRows.length; r++)
    {
        var row = splitedRows[r];
        if (row.indexOf(value) !== -1)
        {

            valueList[r] = [];
            if(row.indexOf("[null,")!= -1)
            {
               row = row.replace("[null,",'[{"value":null,"formatCode":"General"},');
            }
            if(row.indexOf(",null,")!= -1)
            {
               row = row.replace(",null,",'[{"value":null,"formatCode":"General"},');
            }
            var splitedCols = row.split("{");
            for (c = 0; c < splitedCols.length; c++)
            {

                var index = splitedCols[c].indexOf(value);
                if (index !== -1)
                {
                    var str = splitedCols[c].substring(index);
                    var tempVal = str.substring(0, str.indexOf(",") - 1);
                    if(same)
                    {
                        if(value==tempVal)
                            valueList[r][(c - 1)] = tempVal;
                    }
                    else
                        valueList[r][(c - 1)] = tempVal;
                    //console.log("matchingvalue = "+value + " value "+tempVal);
                    //alert("row = "+(r+1)+ " col = "+(c) + " value = "+str.substring(0,str.indexOf(",")-1));
                }
            }
        }
    }
    //alert("valueList "+valueList);
    return valueList;
}

/**
 * Get value of cell from worksheet by passing rowno and ColNo
 * @param {String} worksheetStr (String object of worksheet)
 * @param {Number} rowNo
 * @returns {Number} colNo
 */
function getValue(worksheetStr, rowNo, colNo) {
    var splitedRows = worksheetStr.split("]");

    for (r = 0; r < splitedRows.length; r++)
    {
        
        if (r == rowNo)
        {
            //alert("r "+r +" row "+splitedRows[r]+" rowNo "+rowNo);
            var splitedCols = splitedRows[r].split("{");
            for (c = 0; c < splitedCols.length; c++)
            {
                
                if (c == (colNo + 1))
                {
                    var index = splitedCols[c].indexOf(":");
                    if (index !== -1)
                    {
                        var str = splitedCols[c].substring(index);
                        //console.log("get datavalue row = "+(r+1)+ " col = "+(c) + " value = "+(str.substring(1, (str.indexOf(",")))));
                        return (str.substring(1, str.indexOf(",")));
                    }
                }
            }
        }
    }
}

/**
 * Creates array of data values from worksheet depending of orgunits and dataelemenrs and period.
 * @param {Object} worksheet
 * @param {List} orgUnitData (List of organisation units present in DB)
 * @param {List} dataElementData (List of data elements present in DB)
 * @returns {Array} dataValueJsonArr (Depending on scenarion gets result from respective methods)
 */
function createDataValuesArr(worksheet,orgUnitData,dataElementData) {
    
    var wSDataStr = JSON.stringify(worksheet.data);
    var dataElementInWorkbookList = searchDataElementPosition(worksheet,dataElementData);
    if(dataElementInWorkbookList.length != 0)
    {
        var periodInWorkbookList = searchPeriodPosition(worksheet);
        var orgUnitInWorkbookList = searchOrgUnitPosition(worksheet,orgUnitData);
       // alert("periods " + periodInWorkbookList.length + " ous " + orgUnitInWorkbookList.length + " des = " + dataElementInWorkbookList.length);
        if(periodInWorkbookList.length > 0 && orgUnitInWorkbookList.length > 0 && dataElementInWorkbookList.length > 0)
        {
            searchYearPeriod(worksheet);
            //case 1: single ou, single period  multiple des
            if (periodInWorkbookList.length == 1 && orgUnitInWorkbookList.length == 1)
            {
                return getDataValueFromScenario1(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList);
            }
            else if (periodInWorkbookList.length != 1 && orgUnitInWorkbookList.length == 1) {
                //case 2: single ou, multiple period  multiple des 
                return getDataValueFromScenario2(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList);

            }
            else if (periodInWorkbookList.length == 1 && orgUnitInWorkbookList.length != 1) {
                //case 3: multiple ou, single period  multiple des 
                return getDataValueFromScenario3(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList);

            }
            else if (periodInWorkbookList.length != 1 && orgUnitInWorkbookList.length != 1) {
                //case 4: multiple ou, multiple period  multiple des 
                return getDataValueFromScenario4(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList);
            }
        }
        else
            alert("Sheet is having invalid data");
    }
}

/**
 * Get datavalue JSON array in the case of single ou, single period and  multiple des
 * @param {String} wSDataStr (String obj of worksheet)
 * @param {List} periodInWorkbookList
 * @param {List} orgUnitInWorkbookList
 * @param {List} dataElementInWorkbookList
 * @returns {Array} dataValueJsonArr
 */
function getDataValueFromScenario1(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList){
    var dataValueJsonArr = [];    
    for (rowNo = 0; rowNo < dataElementInWorkbookList.length; rowNo++) {
        if (dataElementInWorkbookList[rowNo] != undefined && dataElementInWorkbookList[rowNo] != "")
        {
            for (col = 0; col < dataElementInWorkbookList[rowNo].length; col++) {
                if (dataElementInWorkbookList[rowNo][col] != undefined && dataElementInWorkbookList[rowNo][col] != "")
                {
                    var value = "";
                    //console.log(" dataElementBase = "+dataElementBase);
            
                    if (dataElementBase == "rowBased")
                        value = getValue(wSDataStr, rowNo, (col + 1));
                    else if (dataElementBase == "colBased")
                        value = getValue(wSDataStr, (rowNo + 1), (col));
                    else
                        value = getValue(wSDataStr, rowNo, (col + 1));

                    if(value!="" && value!=undefined && value!=null && !isNaN(value))
                    {
                        var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                        if(dEMetaData[0] != undefined && orgUnitInWorkbookList[0] != undefined && periodInWorkbookList[0] != undefined && dEMetaData[1] != undefined)
                        {
                            dataValueJsonArr.push({ 
                                "DataElement" : dEMetaData[0],
                                "OrganisationUnit"  : orgUnitInWorkbookList[0],
                                "Period" : periodInWorkbookList[0],
                                "DataValue" : value,
                                "DataElementCode" : dEMetaData[1]
                            });
                        }
                    }
                }
            }
        }
    }
    return dataValueJsonArr;
}

/**
 * Get datavalue JSON array in the case of single ou, multiple period and multiple des 
 * @param {String} wSDataStr (String obj of worksheet)
 * @param {List} periodInWorkbookList
 * @param {List} orgUnitInWorkbookList
 * @param {List} dataElementInWorkbookList
 * @returns {Array} dataValueJsonArr
 */
function getDataValueFromScenario2(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList){
    var dataValueJsonArr = []
    $.each(periodInWorkbookList, function(periodIndex, period) {
        if (period != undefined && period != "")
        {
            for (rowNo = 0; rowNo < dataElementInWorkbookList.length; rowNo++) {
                if (dataElementInWorkbookList[rowNo] != undefined && dataElementInWorkbookList[rowNo] != "")
                {
                    for (col = 0; col < dataElementInWorkbookList[rowNo].length; col++) {
                        if (dataElementInWorkbookList[rowNo][col] != undefined && dataElementInWorkbookList[rowNo][col] != "")
                        {
                            var value = "";

                            if (dataElementBase == "rowBased")
                                value = getValue(wSDataStr, rowNo, periodIndex);
                            else if (dataElementBase == "colBased")
                                value = getValue(wSDataStr, periodIndex, col);

                            if(value!="" && value!=undefined && value!=null && !isNaN(value))
                            {
                                var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                if(dEMetaData[0] != undefined && orgUnitInWorkbookList[0] != undefined && period != undefined && dEMetaData[1] != undefined)
                                {
                                    dataValueJsonArr.push({ 
                                        "DataElement" : dEMetaData[0],
                                        "OrganisationUnit"  : orgUnitInWorkbookList[0],
                                        "Period" : period,
                                        "DataValue" : value,
                                        "DataElementCode" : dEMetaData[1]
                                    });
                                    //alert("de " + dataElementInWorkbookList[rowNo][col] + " period " + period + " ou " + orgUnitInWorkbookList[0] + " actual data " + dataValueArr[key]);
                                }
                            }
                        }
                    }
                }
            }
        }
    });
    return dataValueJsonArr;
}

/**
 * Get datavalue JSON array in the case of multiple ou, single period and multiple des 
 * @param {String} wSDataStr (String obj of worksheet)
 * @param {List} periodInWorkbookList
 * @param {List} orgUnitInWorkbookList
 * @param {List} dataElementInWorkbookList
 * @returns {Array} dataValueJsonArr
 */
function getDataValueFromScenario3(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList){
    var dataValueJsonArr = [];
    $.each(orgUnitInWorkbookList, function(ouIndex, orgUnit) {
        if (orgUnit != undefined && orgUnit != "")
        {
            for (rowNo = 0; rowNo < dataElementInWorkbookList.length; rowNo++) {
                if (dataElementInWorkbookList[rowNo] != undefined && dataElementInWorkbookList[rowNo] != "")
                {
                    for (col = 0; col < dataElementInWorkbookList[rowNo].length; col++) {
                        if (dataElementInWorkbookList[rowNo][col] != undefined && dataElementInWorkbookList[rowNo][col] != "")
                        {
                            var value = "";

                            if (dataElementBase == "rowBased")
                                value = getValue(wSDataStr, rowNo, ouIndex);
                            else if (dataElementBase == "colBased")
                                value = getValue(wSDataStr, ouIndex, col);

                            if(value!="" && value!=undefined && value!=null && !isNaN(value))
                            {
                                var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                if(dEMetaData[0] != undefined && orgUnit != undefined && periodInWorkbookList[0] != undefined && dEMetaData[1] != undefined)
                                {
                                    dataValueJsonArr.push({ 
                                        "DataElement" : dEMetaData[0],
                                        "OrganisationUnit"  : orgUnit,
                                        "Period" : periodInWorkbookList[0],
                                        "DataValue" : value,
                                        "DataElementCode" : dEMetaData[1]
                                    }); 
                                    //alert("de "+dataElementInWorkbookList[rowNo][col]+ " ou "+orgUnit + " period "+ periodInWorkbookList[0] + " actual data "+dataValueArr[key]);
                                }
                            }
                        }
                    }
                }
            }
        }
    });
    return dataValueJsonArr;
}

/**
 * Get datavalue JSON array in the case of multiple ou, multiple period and multiple des 
 * @param {String} wSDataStr (String obj of worksheet)
 * @param {List} periodInWorkbookList
 * @param {List} orgUnitInWorkbookList
 * @param {List} dataElementInWorkbookList
 * @returns {Array} dataValueJsonArr
 */
function getDataValueFromScenario4(wSDataStr,periodInWorkbookList,orgUnitInWorkbookList,dataElementInWorkbookList){
    var dataValueJsonArr = [];
    if(dataElementBase.trim()==periodBase.trim())
    {
        $.each(orgUnitInWorkbookList, function(ouIndex, orgUnit) {
            if (orgUnit != undefined && orgUnit != "")
            {
                for (rowNo = 0; rowNo < dataElementInWorkbookList.length; rowNo++) {
                    if (dataElementInWorkbookList[rowNo] != undefined && dataElementInWorkbookList[rowNo] != "")
                    {
                        for (col = 0; col < dataElementInWorkbookList[rowNo].length; col++) {
                            if (dataElementInWorkbookList[rowNo][col] != undefined && dataElementInWorkbookList[rowNo][col] != "")
                            {
                                if (dataElementBase == "rowBased")
                                {
                                    //Since dataElementBase and periodBase are rowBased get period by passing same rowNo
                                    var value = getValue(wSDataStr, rowNo, ouIndex);
                                    
                                    if(value!="" && value!=undefined && value!=null && !isNaN(value))
                                    {
                                        var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                        if(dEMetaData[0] != undefined && orgUnit != undefined && periodInWorkbookList[rowNo] != undefined && dEMetaData[1] != undefined)
                                        {
                                            dataValueJsonArr.push({ 
                                            "DataElement" : dEMetaData[0],
                                            "OrganisationUnit"  : orgUnit,
                                            "Period" : periodInWorkbookList[rowNo],
                                            "DataValue" : value,
                                            "DataElementCode" : dEMetaData[1]
                                            }); 
                                            //console.log("de "+dataElementInWorkbookList[rowNo][col]+ " ou "+orgUnit + " period "+ periodInWorkbookList[0] + " actual data "+dataValueArr[key]);
                                        }
                                    }
                                }
                                if (dataElementBase == "colBased")
                                {
                                    //Since dataElementBase and periodBase are colBased get period by passin same colNo
                                    var value = getValue(wSDataStr, ouIndex, col)
                                    if(value!="" && value!=undefined && value!=null && !isNaN(value))
                                    {
                                        var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                        if(dEMetaData[0] != undefined && orgUnit != undefined && periodInWorkbookList[col] != undefined && dEMetaData[1] != undefined)
                                        {
                                            dataValueJsonArr.push({ 
                                                "DataElement" : dEMetaData[0],
                                                "OrganisationUnit"  : orgUnit,
                                                "Period" : periodInWorkbookList[col],
                                                "DataValue" : value,
                                                "DataElementCode" : dEMetaData[1]
                                            }); 
                                            //console.log("de " + dataElementInWorkbookList[rowNo][col] + " ou " + orgUnit + " period " + periodInWorkbookList[col] + " actual data " + value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
    else if(dataElementBase.trim()==orgUnitBase.trim())
    {
        $.each(periodInWorkbookList, function(periodIndex, period) {
            if (period != undefined && period != "")
            {
                //alert(" ouIndex = "+ouIndex);
                for (rowNo = 0; rowNo < dataElementInWorkbookList.length; rowNo++) {
                    if (dataElementInWorkbookList[rowNo] != undefined && dataElementInWorkbookList[rowNo] != "")
                    {
                        for (col = 0; col < dataElementInWorkbookList[rowNo].length; col++) {
                            if (dataElementInWorkbookList[rowNo][col] != undefined && dataElementInWorkbookList[rowNo][col] != "")
                            {
                                if (dataElementBase == "rowBased")
                                {
                                    //Since dataElementBase and orgUnitBase are rowBased get orgunit by passing same rowNo
                                    var key = getArrKey(dataElementInWorkbookList[rowNo][col],orgUnitInWorkbookList[rowNo],period);
                                    var value = getValue(wSDataStr, rowNo, periodIndex);
                                    if(value!="" && value!=undefined && value!=null && !isNaN(value))
                                    {
                                        var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                        if(dEMetaData[0] != undefined && orgUnitInWorkbookList[rowNo] != undefined && period != undefined && dEMetaData[1] != undefined)
                                        {
                                            dataValueJsonArr.push({ 
                                                "DataElement" : dEMetaData[0],
                                                "OrganisationUnit"  : orgUnitInWorkbookList[rowNo],
                                                "Period" : period,
                                                "DataValue" : value,
                                                "DataElementCode" : dEMetaData[1]
                                            }); 
                                            //console.log("de "+dataElementInWorkbookList[rowNo][col]+ " ou "+orgUnitInWorkbookList[rowNo] + " period "+ period + " actual data "+dataValueArr[key]);
                                        }
                                    }
                                }
                                if (dataElementBase == "colBased")
                                {
                                    //Since dataElementBase and orgUnitBase are colBased get orgunit by passing same rowNo
                                    var key = getArrKey(dataElementInWorkbookList[rowNo][col],orgUnitInWorkbookList[col],period);
                                    var value = getValue(wSDataStr, periodIndex, col)
                                    if(value!="" && value!=undefined && value!=null && !isNaN(value))
                                    {
                                        var dEMetaData = dataElementInWorkbookList[rowNo][col].split("|");
                                        if(dEMetaData[0] != undefined && orgUnitInWorkbookList[col] != undefined && period != undefined && dEMetaData[1] != undefined)
                                        {
                                            dataValueJsonArr.push({ 
                                                "DataElement" : dEMetaData[0],
                                                "OrganisationUnit"  : orgUnitInWorkbookList[col],
                                                "Period" : period,
                                                "DataValue" : value,
                                                "DataElementCode" : dEMetaData[1]
                                            }); 
                                            //console.log("de " + dataElementInWorkbookList[rowNo][col] + " ou " + orgUnitInWorkbookList[col] + " period " + period + " actual data " + dataValueArr[key]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
    return dataValueJsonArr;
}