var manifestData;
var serverUrl;
var tableData = new Array();
var orgUnitData = new Object();
var dataElementData = new Object();
var excelFile;
var totalMetaDataTypes = 0;

$(document).ready(function() {
    loadManifest();
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
    if (window.File && window.FileReader) {
        $('#tableData').dataTable({

           aoColumns: [
             { "mData": "DataElement", sDefaultContent: "" },
             { "mData": "OrganisationUnit", sDefaultContent: "" },
             { "mData": "Period", sDefaultContent: "" },
             { "mData": "DataValue", sDefaultContent: "" },
             { "mData": "DataElementCode", sDefaultContent: "" ,"bVisible": false }
           ],
           'bFilter': false
       });
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    clearAllData();
});

/**
 * Loads application manifest to scan for activities that map DHIS2 location
 * @returns {undefined}
 */
function loadManifest() {
    jQuery.getJSON('manifest.webapp').done(function(data) {
        manifestData = data;
        serverUrl = manifestData.activities.dhis.href;
        $('#btnExit').attr('href', serverUrl);
        getCurrentUser();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $.blockUI({message: 'Could not load manifest'});
    });
}

/**
 * Gets the current user to verify, if a user has logged in or not.
 * If user is logged in then loads organisationunits and dataelements metadata.
 * @returns {undefined} */
function getCurrentUser() {
    $.ajax({
        url: serverUrl + '/api/currentUser',
        headers: {
            'Accept': 'application/json'
        },
        type: "GET",
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    }).done(function(data, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('Login-Page') == 'true') {
            $.blockUI({message: $('#unauthenticatedMessage')});
        }
        else
        {
            $.blockUI({message: '<h1> Loading...</h1>'});
            loadDhisMetadata('organisationUnits');
            loadDhisMetadata('dataElements');
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $.blockUI({message: $('#failureMessage')});
    });
}

/**
 * 
 * @returns {String}
 */
function loadDhisMetadata(metaDataType) {
    $.ajax({
        url: serverUrl + '/api/metaData?assumeTrue=false&' + metaDataType + '=true',
        headers: {
            'Accept': 'application/json'
        },
        type: "GET",
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    }).done(function(data, textStatus, jqXHR) {
        if (jqXHR.getResponseHeader('Login-Page') == 'true') {
            $.blockUI({message: $('#unauthenticatedMessage')});
        } else {
           setData(metaDataType,data);
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $.blockUI({message: $('#failureMessage')});
    });
}

/**
 * sets metaData in instance variables
 * @param {string} metaDataType
 * @param {string} dataStr
 */
function setData(metaDataType,dataStr){
    console.log("setData ----- "+metaDataType);
    totalMetaDataTypes++;
    if(metaDataType == 'organisationUnits')
    {
        orgUnitData = dataStr;
        //console.log("orgUnitData = "+orgUnitData);
    }
    else if(metaDataType == 'dataElements')
    {
         dataElementData = dataStr;
         //console.log("dataElementData = "+dataElementData);
    }
    if (totalMetaDataTypes == 2)
    {
        $.unblockUI();
    }
}

/**
 * Loads file contents into dataTable
 * @param {type} evt
 * @returns {undefined}
 */
function handleFileSelect(evt) {
    $('#messages').removeClass('hidden');
    $('#tableData').dataTable().fnClearTable();
    $('#workSheetNames').empty();

    var files = evt.target.files; // FileList object
    var xlsFile = files[0]; // FileList object
    if (xlsFile.type.match('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') || xlsFile.type.match('text/xlsx')) {
        $('#messages').html(xlsFile.name + ' - ' + xlsFile.size + ' bytes, last modified: ' + xlsFile.lastModifiedDate);
        var i, f;
        for (i = 0, f = files[i]; i != files.length; ++i) {
            var name = f.name;
            var reader = new FileReader();
            reader.readAsDataURL(f);
            reader.onload = function(e) {
                var data = e.target.result;
                var file = data.substring(data.indexOf(',') + 1);
                excelFile = xlsx(file);
                setDataInDropdown(excelFile.worksheets,'workSheetNames');
                //$('#output').text(JSON.stringify(obj.worksheets[0]));
                populateDataForSheet();
            };
        }
    } else {
        $('#messages').html('Incorrect file type');
    }
}

function populateDataForSheet()
{
    var index = $('#workSheetNames').prop("selectedIndex");
    if(excelFile != undefined && orgUnitData.toString()!=="" && dataElementData.toString()!=="")
    {    
        clearAllData();
        var dataValueJsonArr = createDataValuesArr(excelFile.worksheets[index],orgUnitData.organisationUnits,dataElementData.dataElements);
        $('#tableData').dataTable().fnAddData(dataValueJsonArr);
    }
}

function clearAllData()
{
    $("input[name='ouType']:checked").val("code");
    $('#tableData').dataTable().fnClearTable();
    $("#yearField").attr("value", "");
}

/**
 * Adds data into dropdown
 * @param {string} data
 * @param {string} dropdownName
 */
function setDataInDropdown(data, dropdownName) {
    var dropDownVar = $('#' + dropdownName);
    $(data).each(function()
    {
        var option = $('<option />');
        option.attr('value', $('#'+dropdownName+' > option').length).text(this.name);

        dropDownVar.append(option);
    });
}

/**
 * Create XML from dataTable to import datavalues in the DHIS
 * @calls postBulkDataValues by passing data value set xml.
 */
function importDataValuesAction() {
    //corsSetup();
    var year = $('#yearField').val();
    if (year != undefined && year != "") 
    {
        if(year.length < 4 ||  !( /^([0-9])+$/.test( year ) )){
            showWarning( "Please ener valid year, in YYYY pattern. And click 'Import DataValues'.", 3000);
            return false;
        }
        if(year <= 1990)
        {
            showWarning( "Year must be greater than 1990. And click 'Import DataValues'.", 3000 );
            return false;
        }
        console.log("year is valid");
        tableData = $('#tableData').dataTable();
        var rows = tableData.fnGetNodes();
        var dataValueSetXml = "";
        if(rows.length > 0){
            dataValueSetXml = "<dataValueSet xmlns='http://dhis2.org/schema/dxf/2.0' dataElementIdScheme='code' orgUnitIdScheme='name'>";
            $.each(rows, function(index, tableRow) {
                if(tableData.fnGetData(tableRow, 4)!= "" && tableData.fnGetData(tableRow, 2)!= "" && tableData.fnGetData(tableRow, 1) != "")
                    dataValueSetXml = dataValueSetXml+"<dataValue dataElement=\""+tableData.fnGetData(tableRow, 4)+"\" period=\""+$('#yearField').val()+tableData.fnGetData(tableRow, 2)+"\" orgUnit=\""+tableData.fnGetData(tableRow, 1)+"\" value=\""+tableData.fnGetData(tableRow, 3)+"\"/>";
            });
            dataValueSetXml = dataValueSetXml + "</dataValueSet>";
            //$('#output').text(dataValueSetXml);
            //console.log(dataValueSetXml);
            postBulkDataValues(dataValueSetXml);
        }
        else
            showWarning( "No data to Import'.",3000);
    }
    else
        showWarning( "Please ener valid year, in YYYY pattern. And click 'Import DataValues'.", 3000);
}

function showWarning(msg, timeoutSeconds) {
    var warning = $("#messages1");
    warning.removeClass('hidden');
    warning.css("display", "block");
    warning.html(msg);
    setTimeout(function() {
        warning.fadeOut("slow", function() {
            warning.addClass('hidden');
            warning.css("display", "none");
        });
    }, timeoutSeconds);
}
/**
 * post bulk data values using DHIS web API into DHIS table.
 * @returns {undefined} */
function postBulkDataValues(dataXML){
    var dfr = $.Deferred();
    $.ajax({
        url: serverUrl + '/api/dataValueSets',
        headers: {
            'Content-Type': 'application/xml',
            'Accept' : 'application/xml'
        },
        type: "POST",
        cache: false,
        crossDomain: true,
        data: dataXML,
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            console.log(data.documentElement.childNodes[1].childNodes[0].nodeValue);
            alert(data.documentElement.childNodes[1].childNodes[0].nodeValue);
        }
    });
    return dfr.promise();
}

function corsSetup() {
    $.ajaxSetup({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    });
}

function postUser() {
    $.ajax({
        type: "POST",
        url: serverUrl,
        data: data,
        success: success,
        dataType: dataType
    });
}

var createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(method, url, true);    // Most browsers.
    } else if (typeof XDomainRequest != "undefined") {
        xhr = new XDomainRequest();     // IE8 & IE9
        xhr.open(method, url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
};